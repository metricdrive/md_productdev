import datetime

def change_filename(username,companyname,brand):
    current_datetime = datetime.datetime.now()
    new_filename = '{}_{}_{}_{}_{}_{}_{}.xlsx'.format(
        username,
        companyname,
        brand,
        current_datetime.date(),
        current_datetime.time().hour,
        current_datetime.time().minute,
        current_datetime.time().second
    )
    return new_filename