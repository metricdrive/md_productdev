# Generated by Django 3.2.5 on 2021-08-30 12:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('demo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='datainput_process_data',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=100)),
                ('companyname', models.CharField(max_length=200)),
                ('brand', models.CharField(max_length=200)),
                ('partno', models.CharField(max_length=50)),
                ('partname', models.CharField(max_length=500)),
                ('date', models.DateTimeField()),
                ('qty', models.IntegerField()),
            ],
        ),
    ]
