from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
    firstname = models.CharField(max_length=150,null=False,blank=False)
    lastname = models.CharField(max_length=150,null=False,blank=False)
    username = models.CharField(max_length=150,unique=True,blank=False,null=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['firstname','lastname']

    def __str__(self):
        return '{} {}'.format(self.firstname,self.lastname)


class PersonalInfo(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    phonenumber = models.BigIntegerField()
    partsinventory = models.BigIntegerField()

    def __str__(self):
        return '{} {}'.format(self.user.firstname,self.user.lastname)


class Company(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    companyname = models.CharField(max_length=150)
    city = models.CharField(max_length=150)
    category = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} {} -> {}'.format(self.user.firstname,self.user.lastname,self.companyname)


class Brand(models.Model):
    companyname = models.ForeignKey(Company,on_delete=models.CASCADE)
    brand = models.CharField(max_length=150)

    def __str__(self):
        return '{} '.format(self.companyname.companyname)


class sales_data(models.Model):
    username = models.CharField(max_length=100)
    companyname = models.CharField(max_length=200)
    brand = models.CharField(max_length=200)
    partno = models.CharField(
        max_length=50,
        null=False,
    )
    partname = models.CharField(
        max_length=500
    )
    date = models.DateTimeField()
    qty = models.IntegerField()

    def __str__(self):
        return '{}'.format(self.partno)


class backorder_data(models.Model):
    username = models.CharField(max_length=100)
    companyname = models.CharField(max_length=200)
    brand = models.CharField(max_length=200)
    partno = models.CharField(
        max_length=50,
        null=False,
    )
    date = models.DateTimeField()
    qty = models.IntegerField()

    def __str__(self):
        return '{}'.format(self.partno)


class inventory_data(models.Model):
    username = models.CharField(max_length=100)
    companyname = models.CharField(max_length=200)
    brand = models.CharField(max_length=200)
    partno = models.CharField(
        max_length=50,
        null=False,
    )
    date = models.DateTimeField()
    qty = models.IntegerField()
    rate = models.FloatField()

    def __str__(self):
        return '{}'.format(self.partno)


