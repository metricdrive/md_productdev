from django.contrib import auth
from .models import User
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password

def signup_verify(data):
    firstname = data['firstname']
    lastname = data['lastname']
    username = data['username']
    password1 = data['password1']
    password2 = data['password2']

    try:
        User.objects.get(username=username)
        return "user exist"
    except:
        if password1 == password2:
            if len(password1) >= 8:
                if firstname and lastname and username not in ['',' ']:
                    current_user = User.objects.create(
                        username=username,
                        firstname=firstname,
                        lastname=lastname,
                        password=make_password(password1)
                    )
                    return current_user
                else:
                    return "value not acceptable"
            else:
                return "password>8"
        else:
            return "password not matched"