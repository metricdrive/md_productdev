# custom import
from . import forecasting_process
# from mohit.abc import abc

from . import models
from . import verify,personalinfostore
from .datainput_processfile import processdata_main
from .dashboard_processfile import get_process_data,create_dataframe_from_database
from .filename_change import change_filename

from softui.settings import BASE_DIR

# from django.conf import settings


# python import
# import pandas as pd
import os
from os import listdir


# django import
from django.contrib.auth import authenticate,login,logout
from django.core.files.storage import default_storage, FileSystemStorage
from django.core.files.base import ContentFile

# models import
from .models import Company,Brand, sales_data

# rest framework import
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from rest_framework.permissions import IsAuthenticated


from django.db.models import Max,Min


# simple jwt import

# Create your views here.

# client_url = 'http://127.0.0.1:5500'


# sign in functionality
class Signin(APIView):


    def post(self,request):
        try:
            username = request.data['username']
            password = request.data['password']
        except:
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'error' : 'username password can\'t be empty'
                }
            )
        user = authenticate(username=username,password=password)
        if user is None:
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'error' : 'wrong credentials'
                }
            )
        else:
            login(request=request,user=user)
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'success' : "successfully signin",
                    'username' : user.username,
                }
                )


# sign up functionality
class Signup(APIView):

    # @method_decorator(ensure_csrf_cookie)
    def post(self,request):

        data = verify.signup_verify(request.data)

        if data == "user exist":
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'error' : "user exist"
                }
            )
        elif data == "value not acceptable":
                return Response(
                status=status.HTTP_200_OK,
                data={
                    'error' : "null values are not acceptable"
                }
            )
        elif data == "password>8":
                return Response(
                status=status.HTTP_200_OK,
                data={
                    'error' : "password must be minimum 8 character"
                }
            )
        elif data == "password not matched":
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "error" : "password must match"
                }
            )
        else:
            login(request=request,user=data)
            return Response(
                status=status.HTTP_201_CREATED,
                data={
                    'success': "user created",
                    'username' : data.username,
                }
            )


# logout 
class Logout(APIView):

    permission_classes =[IsAuthenticated]
    
    def get(self,request):
        logout(request=request)
        return Response(
            status=status.HTTP_200_OK
        )


# auto login check
class AutoLogin(APIView):

    permission_classes = [IsAuthenticated]
    def get(self,request):
        return Response(
            status=status.HTTP_200_OK,
        )


#  personal info add  -> signup
class personalinfo(APIView):

    permission_classes = [IsAuthenticated]

    def post(self,request):
        try:
            data = personalinfostore.personalinfo_store(request.user,request.data)
            if data == False:
                return Response(
                    status=status.HTTP_200_OK,
                    data={
                        'error' : "wrong type of data"
                    }
                )
            elif data == "company brand empty":
                return Response(
                    status=status.HTTP_200_OK,
                    data={
                        "error" : "company and related brand not present"
                    }
                )
            elif data == "blank fields":
                return Response(
                    status=status.HTTP_200_OK,
                    data={
                        "error" : "fields are blank"
                    }
                )
            elif data == True:
                return Response(
                    status=status.HTTP_201_CREATED,
                    data={
                        "success" : "profile info updated"
                    }
            )
        except:
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "error" : "something went wrong while updating"
                }
            )


# company and brand details
class companybrand(APIView):
    permission_classes = [IsAuthenticated]

    def get(self,request):
        
        current_user = request.user
        try:
            current_user_company = list(models.Company.objects.filter(
                user=current_user
            ))
            data = []
            for company in current_user_company:
                brand = list(Brand.objects.filter(companyname=company))
                brand_data = []
                for values in brand:
                    brand_data.append(values.brand)
                data.append(
                    {
                        "companyname" : company.companyname,
                        "brand" : brand_data
                    }
                )
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "details" : data
                }
            )
        except:
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "error" : "something went wrong while retrieving the data"
                }
            )


# file upload or data input 
class fileupload(APIView):

    permission_classes = [IsAuthenticated]

    # get request -> giving user all the company list
    # and their respective brand
    # and storing them in localstorage in browser
    def get(self,request):
        company_brand ={}
        current_user = request.user
        try:
            company = Company.objects.filter(user=current_user)
        except:
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'error' : 'no company found',
                    'company_brand' : ''
                }
            )
        for value in company:
            brand = []
            brand_lst = Brand.objects.filter(companyname=value)
            for brands in brand_lst:
                brand.append(brands.brand)
            company_brand.update(
                {
                    value.companyname : brand
                }
            )
        return Response(
            status=status.HTTP_200_OK,
            data={
                'success' : 'done',
                'company_brand' : company_brand
            }
        )

    # saving the data is xl format in media
    def savefile(self,data,username):
        
        datatype = data['datatype']
        datasource = data['datasource']
        brand = data['brand']
        xlfile = data['file']
        companyname = data['companyname'].strip()

        if datatype and datasource and brand != False:
            if xlfile != False:
                if (
                    xlfile.content_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    or xlfile.content_type == 'application/vnd.ms-excel'
                ):
                    if xlfile.name.endswith('.xlsx') or xlfile.name.endswith('.xls'):
                        extension = xlfile.name
                        path = default_storage.save(
                            change_filename(username,companyname,brand),
                            ContentFile(
                                xlfile.read()
                            )
                        )
                        return path
                    else:
                        return 'file not supported'
                else:
                    return "file not supported"
            else:
                return "no file"
        else:
            return "blank field"

    # post request
    def post(self,request):
        result = self.savefile(data=request.data,username=request.user.username)
        if result == "blank field":
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'error' : 'field\'s can\'t be blank'
                }
            )
        elif result == 'no file':
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'error' : 'no file uploaded'
                }
            )
        elif result == 'file not supported':
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'error' : 'file not supported'
                }
            )
        # processing file in processfie.py
        else:
            brand = request.data['brand']
            companyname = request.data['companyname'].strip()
            datatype = request.data['datatype']
            process_result = processdata_main(
                request.user.username,
                companyname,
                brand,
                datatype,
                default_storage.location,
                result,
            )
            if process_result == True:
                return Response(
                    status=status.HTTP_200_OK,
                    data={
                        'success' : 'successfully processed'
                    }
                )
            elif result == 'file not found':
                return Response(
                    status=status.HTTP_200_OK,
                    data={
                        'error' : 'file not found'
                    }
                )
            else:
                return Response(
                    status=status.HTTP_200_OK,
                    data={
                        'error' : 'something went wrong'
                    }
                )


# forecasting 
class forecasting(APIView):

    permission_classes = [IsAuthenticated]

    # save the data
    def savefile(self,data,username):
        
        companyname = data['companyname'].lower()
        brand = data['brand'].lower()
        forecast_period = data['forecast_period']
        target = data['target']
        lead_time = data['lead_time']
        xlfile = data['file']
        # print(target*lead_time)
        if forecast_period and target and brand and lead_time != False:
            if xlfile != False:
                if (
                    xlfile.content_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    or xlfile.content_type == 'application/vnd.ms-excel'
                ):
                    if xlfile.name.endswith('.xlsx') or xlfile.name.endswith('.xls'):
                        new_filename = change_filename(username,companyname,brand)
                        fs = FileSystemStorage(
                            location=os.path.join(BASE_DIR,'mohit/inbound')
                        )
                        fs.save(name=new_filename,content=xlfile)
                        result = forecasting_process.process_file(
                            forecast_period,
                            target,
                            lead_time,
                            username,
                            companyname,
                            brand
                        )
                        result = True
                        if result:
                            return True
                        else:
                            return result
                    else:
                        return 'file not supported'
                else:
                    return "file not supported"
            else:
                return "no file"
        else:
            return "blank field"

    def get(self,request):
        return Response(
            status=status.HTTP_200_OK,
            data={
                'success':'done'
            }
        )
    
    def post(self,request):
        
        result =  self.savefile(request.data,request.user.username)
        if result == True:   
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "success" : 'successfully processed'
                }
            )
        else:
            return Response(
            status=status.HTTP_200_OK,
            data={
                'error' : result
            }
        )


# dashboard
class dasboard(APIView):

    permission_classes = [IsAuthenticated]


    # def bal(self,username,companyname,brand):
    #     return True
    
    def post(self,request):
        # result = get_process_data(request.user.username,request.data)
        result = create_dataframe_from_database(
            request.user.username,
            request.data["companyname"],
            request.data["brand"]
        )
        # print()
        if result:
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'success' : 'done',
                    "data" : result
                }
            )
        else:
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'success' : 'done',
                    "data" : "No details found"
                }
            )


# reports
class reports(APIView):

    def getfile(self,username,companyname,brand,filepath):
        
        selected_file = []
        for file in listdir(filepath):
            if file.startswith('{}_{}_{}'.format(username.lower(),companyname.lower(),brand.lower())):
                selected_file.append(file)
        try:
            return selected_file
        except:
            return False

    def post(self,request):
        filepath = os.path.join(BASE_DIR,'mohit/reports')
        if request.data["file"] == "raw":
            file = self.getfile(request.user.username,request.data['companyname'],request.data['brand'],filepath)
            return Response(
                status=status.HTTP_200_OK,
                data={
                "files" : file
                }
            )
        elif request.data["file"] == "target":
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "success" : "successfully send target data"
                }
            )
        else:
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "error" : "no data found"
                }
            )


# profile information
class profile(APIView):

    permission_classes = [IsAuthenticated]

    def get(self,request):
        curret_user = request.user
        firstname = curret_user.firstname
        lastname = curret_user.lastname
        username = curret_user.username
        phonenumber = models.PersonalInfo.objects.get(user=curret_user).phonenumber
        return Response(
            status=status.HTTP_200_OK,
            data={
                'success' : 'done',
                "firstname" : firstname,
                "lastname" : lastname,
                "username" : username,
                "phonenumber" : phonenumber
            }
        )