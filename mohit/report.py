import pandas as pd



def generate_report(df,months):
    report=[]
    
    # Total Sale
    total_value=df['Total Value'].sum()
    report.append(total_value)
    # Avg Monthly Sale
    avg_monthly_sale=total_value/months
    report.append(avg_monthly_sale)
    # Inventory Value
    inventory_value= 
    report.append(inventory_value)
    # Inventory Days
    report.append((inventory_value/avg_monthly_sale)*30)
    # Serviced Parts
    report.append(df['Part No'].nunique())
    # Unserviced Parts
    report.append()
    # Serviced Part Inventory
    report.append(df['Inventory Value'].sum())
    # Unserviced Part Inventory
    report.append()
    # ABC count
    report.append(df.loc[df['ABC']=='A','ABC'].count())
    report.append(df.loc[df['ABC']=='B','ABC'].count())
    report.append(df.loc[df['ABC']=='C','ABC'].count())
    report.append(df.loc[df['ABC']=='D','ABC'].count())
    # FMS Count
    report.append(df.loc[df['FMS']=='F','FMS'].count())
    report.append(df.loc[df['FMS']=='M','FMS'].count())
    report.append(df.loc[df['FMS']=='S','FMS'].count())
    report.append(df.loc[df['FMS']=='Z','FMS'].count())
    # Category Count
    report.append(df.loc[df['Category']=='Smooth','Category'].count())
    report.append(df.loc[df['Category']=='Eratic','Category'].count())
    report.append(df.loc[df['Category']=='Intermittent','Category'].count())
    report.append(df.loc[df['Category']=='Lumpy','Category'].count())
    report.append(df.loc[df['Category']=='Not Defined','Category'].count())
    # Non Moving Count
    report.append(df.loc[df['Non Moving']=='270 Days')
    
    


